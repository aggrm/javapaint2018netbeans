/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package codigo;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JToggleButton;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author xp
 */
public class VentanaPaint extends javax.swing.JFrame 
{
    //Inicio los bufers y formas---------------------------------------------------------
    BufferedImage buffer, buffer2 = null; 
    Color colorSeleccionado = Color.BLACK; 
    Forma miForma; 
    //-----------------------------------------------------------------------------------
    
    //Iniciamos la forma por defecto-----------------------------------------------------
    int formaSeleccionada = 3;  // si vale 100 pinto circulos
                                // si vale 4 pinto cuadrados
                                // si vale 3 pinto triangulos
                                // si vale 5 pinto pentágonos
    //-----------------------------------------------------------------------------------
    
    //Damos los graficos a los buffers y al jPanel para qe pueda hacer formas------------
    Graphics2D bufferGraphics, buffer2Graphics, jPanelGraphics = null;
    //-----------------------------------------------------------------------------------
    
    //Inicializo los strokes para que pueda hacer trazos diferentes----------------------
    BasicStroke trazo1 = new BasicStroke(15);
    BasicStroke trazo2 = new BasicStroke(15,
                                        BasicStroke.CAP_BUTT,
                                        BasicStroke.JOIN_MITER, 
                                        10.0f, 
                                        new float[]{10.0f}, 
                                        0.0f);
    //------------------------------------------------------------------------------------
    
    //Para el la traza de mano alzada----------------------------------------------------
    int x;
    int y;
    int x2;
    int y2;
    //------------------------------------------------------------------------------------
    
    public VentanaPaint() 
    {
        //----------------------------------------------------------------------
        initComponents();                                                       //Iniciamos todos los componentes
        jLabel1.setBackground(Color.ORANGE);                                    //Damos un color al label
        inicializaBuffers();                                                    //Inicializo los Buffers 1 y 2
        jDialog1.setSize(640, 470);                                             //Damos un yamaño al jDialog de los colores
        this.setTitle("Paint De Goujon el Franchute");                          //Damos un título a nuestro paint
        //----------------------------------------------------------------------
        
        //----------------------------------------------------------------------
        FileNameExtensionFilter filtro = 
                new FileNameExtensionFilter("JPG y PNG sólo", "jpg", "png");    //Para que por defecto te seleccione JPG y PNG
        jFileChooser1.setFileFilter(filtro);                                    //Para poder decidir entre JPG y PNG 
        //----------------------------------------------------------------------
    }

    private void inicializaBuffers()
    {
        //creo una imagen del mismo ancho y alto que el lienzo------------------
        buffer = (BufferedImage) jPanel1.createImage(jPanel1.getWidth(), 
                jPanel1.getHeight());
        //----------------------------------------------------------------------
        
        //creo una imagen modificable-------------------------------------------
        bufferGraphics = buffer.createGraphics();
        //----------------------------------------------------------------------
        
        //incializa el buffer para que sea un rectángulo rojo que ocupe todo el 
        //jpanel
        bufferGraphics.setColor(Color.white);
        bufferGraphics.fillRect(0, 0, buffer.getWidth(), buffer.getHeight());
        //----------------------------------------------------------------------
        
        //inicializo el 2º Buffer-----------------------------------------------
         buffer2 = (BufferedImage) jPanel1.createImage(jPanel1.getWidth(), 
                 jPanel1.getHeight());
        //----------------------------------------------------------------------
         
        //creo una imagen modificable-------------------------------------------
        buffer2Graphics = buffer2.createGraphics();
        //----------------------------------------------------------------------
        
        //incializa el buffer para que sea un rectángulo blanco que ocupe todo 
        //el jpanel
        buffer2Graphics.setColor(Color.white);
        buffer2Graphics.fillRect(0, 0, buffer2.getWidth(), buffer2.getHeight());    
        //----------------------------------------------------------------------
        
        jPanelGraphics = (Graphics2D) jPanel1.getGraphics();
    }
    
    @Override
    public void paint(Graphics g)
    {
        super.paint(g);
        //pinto el buffer sobre el jFrame---------------------------------------
        jPanelGraphics.drawImage(buffer, 0, 0, null);
        //----------------------------------------------------------------------
    }
    
    
    private void deSelecciona()
    {
        Component[] components = (Component[]) getContentPane().getComponents();
        for (Component comp : components) {
            if (comp instanceof JToggleButton) {
                ((JToggleButton)comp).setSelected(false);
            }
        } 
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDialog1 = new javax.swing.JDialog();
        jColorChooser1 = new javax.swing.JColorChooser();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jDialog2 = new javax.swing.JDialog();
        jFileChooser1 = new javax.swing.JFileChooser();
        jPanel1 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jLabel1 = new javax.swing.JLabel();
        circulo = new javax.swing.JToggleButton();
        cuadrado = new javax.swing.JToggleButton();
        linea = new javax.swing.JToggleButton();
        triangulo = new javax.swing.JToggleButton();
        hexagono = new javax.swing.JToggleButton();
        serendipia = new javax.swing.JToggleButton();
        grosor = new javax.swing.JSlider();
        grosor2 = new javax.swing.JSpinner();
        jCheckBox2 = new javax.swing.JCheckBox();
        jCheckBox3 = new javax.swing.JCheckBox();
        lapiz = new javax.swing.JToggleButton();
        goma = new javax.swing.JToggleButton();
        decagono = new javax.swing.JToggleButton();
        decagono1 = new javax.swing.JToggleButton();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuItem2 = new javax.swing.JMenuItem();

        jButton1.setText("Cancelar");
        jButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton1MousePressed(evt);
            }
        });

        jButton2.setText("Guardar");
        jButton2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jButton2MousePressed(evt);
            }
        });

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jColorChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 650, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addGap(115, 115, 115)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jButton2)
                .addGap(175, 175, 175))
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jColorChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, 344, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31)
                .addGroup(jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(jButton2))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jDialog2Layout = new javax.swing.GroupLayout(jDialog2.getContentPane());
        jDialog2.getContentPane().setLayout(jDialog2Layout);
        jDialog2Layout.setHorizontalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        jDialog2Layout.setVerticalGroup(
            jDialog2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jDialog2Layout.createSequentialGroup()
                .addComponent(jFileChooser1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 58, Short.MAX_VALUE))
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setCursor(new java.awt.Cursor(java.awt.Cursor.CROSSHAIR_CURSOR));
        jPanel1.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jPanel1MouseDragged(evt);
            }
        });
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jPanel1MousePressed(evt);
            }
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                jPanel1MouseReleased(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1019, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        jCheckBox1.setText("relleno");

        jLabel1.setBackground(new java.awt.Color(255, 255, 255));
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/colres.png"))); // NOI18N
        jLabel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jLabel1MousePressed(evt);
            }
        });

        circulo.setBackground(new java.awt.Color(255, 255, 255));
        circulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/circulo.png"))); // NOI18N
        circulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                circuloMousePressed(evt);
            }
        });

        cuadrado.setBackground(new java.awt.Color(255, 255, 255));
        cuadrado.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cuadrado.png"))); // NOI18N
        cuadrado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                cuadradoMousePressed(evt);
            }
        });

        linea.setBackground(new java.awt.Color(255, 255, 255));
        linea.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/linea.png"))); // NOI18N
        linea.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lineaMousePressed(evt);
            }
        });

        triangulo.setBackground(new java.awt.Color(255, 255, 255));
        triangulo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/triangulo.png"))); // NOI18N
        triangulo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                trianguloMousePressed(evt);
            }
        });

        hexagono.setBackground(new java.awt.Color(255, 255, 255));
        hexagono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hexagono.png"))); // NOI18N
        hexagono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                hexagonoMousePressed(evt);
            }
        });

        serendipia.setBackground(new java.awt.Color(255, 255, 255));
        serendipia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/jorge.jpg"))); // NOI18N
        serendipia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                serendipiaMousePressed(evt);
            }
        });

        grosor.setMinimum(1);
        grosor.setOrientation(javax.swing.JSlider.VERTICAL);
        grosor.setValue(10);
        grosor.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        grosor.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                grosorMouseDragged(evt);
            }
        });

        grosor2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                grosor2StateChanged(evt);
            }
        });

        jCheckBox2.setText("Modo puntas redondeadas");

        jCheckBox3.setText("Modo croqueta");

        lapiz.setBackground(new java.awt.Color(255, 255, 255));
        lapiz.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Lapiz-01.png"))); // NOI18N
        lapiz.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                lapizMousePressed(evt);
            }
        });

        goma.setBackground(new java.awt.Color(255, 255, 255));
        goma.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/goma.png"))); // NOI18N
        goma.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                gomaMousePressed(evt);
            }
        });

        decagono.setBackground(new java.awt.Color(255, 255, 255));
        decagono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/decagono.png"))); // NOI18N
        decagono.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                decagonoMousePressed(evt);
            }
        });

        decagono1.setBackground(new java.awt.Color(255, 255, 255));
        decagono1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/jorge.jpg"))); // NOI18N
        decagono1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                decagono1MousePressed(evt);
            }
        });

        jMenu1.setText("File");
        jMenu1.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jMenu1StateChanged(evt);
            }
        });

        jMenuItem1.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem1.setText("Guardar");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem1MousePressed(evt);
            }
        });
        jMenu1.add(jMenuItem1);

        jMenuItem2.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_O, java.awt.event.InputEvent.CTRL_MASK));
        jMenuItem2.setText("Cargar");
        jMenuItem2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenuItem2MousePressed(evt);
            }
        });
        jMenu1.add(jMenuItem2);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jCheckBox3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBox1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jCheckBox2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(decagono1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(linea, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(triangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(layout.createSequentialGroup()
                                            .addComponent(hexagono, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(serendipia, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addComponent(decagono, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(circulo, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(cuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(lapiz, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(goma, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addComponent(grosor2, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(grosor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jCheckBox1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jCheckBox3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(circulo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cuadrado, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(linea, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(triangulo, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(hexagono, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(serendipia, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(decagono, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lapiz, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(decagono1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(goma, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(grosor2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(grosor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(771, 771, 771))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jPanel1MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseDragged
        switch (formaSeleccionada)                                              //Creo el switch para que el trazo a mano alzada funcione
        {
            case 10:                                                            //Hace el lapiz
                x2 = evt.getX();
                y2 = evt.getY();
                if (x != x2 || y != y2) 
                {
                   bufferGraphics.setColor(colorSeleccionado);                  //Para seleccionar el color
                   bufferGraphics.drawLine(x, y, x2, y2);                       //Me hace la linea
                   bufferGraphics.setStroke(new Trazo                           //Para que me dibiuje circulos
                            (grosor.getValue(), 1.0f, 1.0f));
                    buffer2Graphics.setColor(colorSeleccionado);                //Selecciona los colores para el buffer2
                    buffer2Graphics.drawLine(x, y, x2, y2);                     //Me hace la linea
                    buffer2Graphics.setStroke(new Trazo                         //Para que me dibuje círculos
                            (grosor.getValue(), 1.0f, 1.0f));
                   
                   
                   x = x2;                                                      //Para que me dibuje donde el raton en x                                      
                   y = y2;                                                      //Para que me dibuje donde el raton en y
                }
                break;
                
            case 11:                                                            //Hace la goma de borrar
                x2 = evt.getX();
                y2 = evt.getY();
                if (x != x2 || y != y2) 
                {
                    bufferGraphics.setColor(Color.white);
                    bufferGraphics.drawLine(x, y, x2, y2);
                    bufferGraphics.setStroke(new Trazo                          //Hace que sea rendondo la mano alzada
                        (grosor.getValue(), 1.0f, 1.0f));
                    buffer2Graphics.setColor(Color.white);
                    buffer2Graphics.drawLine(x, y, x2, y2);
                    buffer2Graphics.setStroke(new Trazo                         //Para que me dibuje círculos
                            (grosor.getValue(), 1.0f, 1.0f));
                    
                    
                    x = x2;
                    y = y2;
                }
                break;
        
        default:
        bufferGraphics.drawImage(buffer2, 0, 0, null);
        //Mdodo puntas redondas-------------------------------------------------
        if(jCheckBox2.isSelected())
        {
            miForma.dibujate(bufferGraphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue(), 20.0f, 10.0f));       
        }
        //----------------------------------------------------------------------
        
        //Modo croquetas--------------------------------------------------------
        if(jCheckBox3.isSelected())
        {
            miForma.dibujate(bufferGraphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue(), 90.0f, 50.0f));       
        }
        else
        {
            miForma.dibujate(bufferGraphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue()));       
        }
        //----------------------------------------------------------------------

                

                break;
        }
        
        repaint(0,0,1,1);      
    }//GEN-LAST:event_jPanel1MouseDragged
                     
    private void jPanel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MousePressed
        //inicializo la forma que usaré para dibujar en el buffer---------------
        switch (formaSeleccionada) {
            case 2:                                                             //Hace líneas
                miForma = new Linea(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 3:                                                             //Hace triangulos
                miForma = new Triangulo(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 4:                                                             //Hace cuadrados
                miForma = new Cuadrado(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 5:                                                             //Hace pentagonos
                miForma = new Pentagono(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 6:                                                             //Hace decagonos
                miForma = new Decagono(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 7:                                                             //Hace estrella diferente
                miForma = new Estrella2(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 10:                                                            //Hace el lapiz
                x = evt.getX();
                y = evt.getY();
                break;
            case 11:                                                            //Hace la goma de borrar
                x = evt.getX();
                y = evt.getY();
                break;
            case 24:                                                            //Hace la estrella
                miForma = new Estrella(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
            case 100:                                                           //Hace el circulo
                miForma = new Circulo(evt.getX(), evt.getY(), 
                        colorSeleccionado, jCheckBox1.isSelected());
                break;
        }      
    }//GEN-LAST:event_jPanel1MousePressed

    private void jPanel1MouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseReleased
        switch (formaSeleccionada)                                              //ceo el switch para que el trazo a mano alzada funcione
        {
            case 10:
                x2 = evt.getX();
                y2 = evt.getY();
                if (x != x2 || y != y2) 
                {
                   bufferGraphics.setColor(colorSeleccionado);                  //Para seleccionar el color
                   bufferGraphics.drawLine(x, y, x2, y2);                       //Me hace la linea
                   bufferGraphics.setStroke(new Trazo                           //Para que me dibiuje circulos
                            (grosor.getValue(), 1.0f, 1.0f));
                    buffer2Graphics.setColor(colorSeleccionado);                //Selecciona los colores para el buffer2
                    buffer2Graphics.drawLine(x, y, x2, y2);                     //Me hace la linea
                    buffer2Graphics.setStroke(new Trazo                         //Para que me dibuje círculos
                            (grosor.getValue(), 1.0f, 1.0f));
                   
                   
                   x = x2;                                                      //Para que me dibuje donde el raton en x                                      
                   y = y2;                                                      //Para que me dibuje donde el raton en y
                }
                break;
                
            case 11:
                x2 = evt.getX();
                y2 = evt.getY();
                if (x != x2 || y != y2) 
                {
                    bufferGraphics.setColor(Color.white);
                    bufferGraphics.drawLine(x, y, x2, y2);
                    bufferGraphics.setStroke(new Trazo                          //Hace que sea rendondo la mano alzada
                        (grosor.getValue(), 1.0f, 1.0f));
                    buffer2Graphics.setColor(Color.white);
                    buffer2Graphics.drawLine(x, y, x2, y2);
                    buffer2Graphics.setStroke(new Trazo                         //Para que me dibuje círculos
                            (grosor.getValue(), 1.0f, 1.0f));
                    
                    
                    x = x2;
                    y = y2;
                }
                break;
        default:
        //Mdodo puntas redondas-------------------------------------------------
        if(jCheckBox2.isSelected())
        {
            miForma.dibujate(buffer2Graphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue(), 20.0f, 10.0f));       
        }
        //----------------------------------------------------------------------
        
        //Modo croquetas--------------------------------------------------------
        if(jCheckBox3.isSelected())
        {
            miForma.dibujate(buffer2Graphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue(), 90.0f, 50.0f));       
        }
        else
        {
            miForma.dibujate(buffer2Graphics, evt.getY(), evt.getX(),
                    new Trazo(grosor.getValue()));       
        }
        //----------------------------------------------------------------------

                

                break;
        }
    }//GEN-LAST:event_jPanel1MouseReleased

    private void jLabel1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MousePressed
        jDialog1.setVisible(true);
    }//GEN-LAST:event_jLabel1MousePressed

    private void jButton1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton1MousePressed
        jDialog1.setVisible(false);
    }//GEN-LAST:event_jButton1MousePressed

    private void jButton2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButton2MousePressed
        //Metodo donde seleccionamos el color de las formas---------------------
        jDialog1.setVisible(false);
        colorSeleccionado = jColorChooser1.getColor();
        jLabel1.setBackground(colorSeleccionado);
        
    }//GEN-LAST:event_jButton2MousePressed

    private void circuloMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_circuloMousePressed
        //elige circulos--------------------------------------------------------
        formaSeleccionada = 100;
        deSelecciona();
    }//GEN-LAST:event_circuloMousePressed

    private void cuadradoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cuadradoMousePressed
        //elige cuadrados-------------------------------------------------------
        formaSeleccionada = 4;        
        deSelecciona();
    }//GEN-LAST:event_cuadradoMousePressed

    private void trianguloMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_trianguloMousePressed
        //elige triangulos------------------------------------------------------
        formaSeleccionada = 3;
        deSelecciona();
    }//GEN-LAST:event_trianguloMousePressed

    private void hexagonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_hexagonoMousePressed
        //elige hexagonos-------------------------------------------------------
        formaSeleccionada = 5;
        deSelecciona();
    }//GEN-LAST:event_hexagonoMousePressed

    private void serendipiaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_serendipiaMousePressed
        //elige serendipia de Jorge---------------------------------------------
        formaSeleccionada = 24;
        deSelecciona();
    }//GEN-LAST:event_serendipiaMousePressed

    private void jMenuItem1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MousePressed
        //Este metodo es el menu del File---------------------------------------
        int seleccion = jFileChooser1.showSaveDialog(this);
        if (seleccion == JFileChooser.APPROVE_OPTION)                           //si llego aquí es que el usuario ha pulsado en "guardar" cuando ha salido 
        {                                                                       //el menú del jFileChooser
         
            File fichero = jFileChooser1.getSelectedFile();
            String nombre = fichero.getName();
            String extension = nombre.substring(nombre.lastIndexOf('.')+1);
            if (extension.equalsIgnoreCase("jpg") || 
                    extension.equalsIgnoreCase("png"))
            {
                try
                {
                    ImageIO.write(buffer, extension, fichero);
                }
                catch(IOException e)
                {
                }
            }
        }
        
    }//GEN-LAST:event_jMenuItem1MousePressed

    private void jMenu1StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jMenu1StateChanged
        //Para que nos repinte el bufer de nuevo--------------------------------
        JMenu menu = (JMenu) evt.getSource();
        if (!menu.isSelected()){
            repaint();
        }
        
    }//GEN-LAST:event_jMenu1StateChanged

    private void jMenuItem2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem2MousePressed
        //Este metodo esta hecho para el guardado de las obra de are del usuario
        int seleccion = jFileChooser1.showOpenDialog(this);
        
        if (seleccion == JFileChooser.APPROVE_OPTION)                           //si llego aquí es que el usuario ha pulsado en "guardar" cuando ha salido 
        {                                                                       //el menú del jFileChooser
            File fichero = jFileChooser1.getSelectedFile();
            String nombre = fichero.getName();
            String extension = nombre.substring(nombre.lastIndexOf('.')+1);
            BufferedImage imagen = null;
            if (extension.equalsIgnoreCase("jpg") || 
                    extension.equalsIgnoreCase("png"))
            {
                try
                {
                    imagen = ImageIO.read(fichero);
                    bufferGraphics.drawImage(imagen, 0, 0, null);
                    buffer2Graphics.drawImage(imagen, 0, 0, null);
                    repaint();
                }
                catch(IOException e)
                {
                }
            }
        }
    }//GEN-LAST:event_jMenuItem2MousePressed

    private void grosorMouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_grosorMouseDragged
        //Este metodo es para el grosor de las lineas pero mas preciso
        grosor2.setValue(grosor.getValue());
    }//GEN-LAST:event_grosorMouseDragged

    private void grosor2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_grosor2StateChanged
        //Este metodo es para el grosor de las lineas pero menos preciso
        grosor.setValue((int) grosor2.getValue());
    }//GEN-LAST:event_grosor2StateChanged

    private void lapizMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lapizMousePressed
        //elige el trazo a mano alzada------------------------------------------
        formaSeleccionada = 10;
        deSelecciona();
    }//GEN-LAST:event_lapizMousePressed

    private void gomaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_gomaMousePressed
        //elige la goma de borrar-----------------------------------------------
        formaSeleccionada = 11;
        deSelecciona();
    }//GEN-LAST:event_gomaMousePressed

    private void lineaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lineaMousePressed
        //elige lineas----------------------------------------------------------
        formaSeleccionada = 2;
        deSelecciona();
    }//GEN-LAST:event_lineaMousePressed

    private void decagonoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_decagonoMousePressed
        formaSeleccionada = 6;
        deSelecciona();
    }//GEN-LAST:event_decagonoMousePressed

    private void decagono1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_decagono1MousePressed
        formaSeleccionada = 7;
        deSelecciona();
    }//GEN-LAST:event_decagono1MousePressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VentanaPaint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VentanaPaint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VentanaPaint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VentanaPaint.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VentanaPaint().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JToggleButton circulo;
    private javax.swing.JToggleButton cuadrado;
    private javax.swing.JToggleButton decagono;
    private javax.swing.JToggleButton decagono1;
    private javax.swing.JToggleButton goma;
    private javax.swing.JSlider grosor;
    private javax.swing.JSpinner grosor2;
    private javax.swing.JToggleButton hexagono;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JCheckBox jCheckBox3;
    private javax.swing.JColorChooser jColorChooser1;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JDialog jDialog2;
    private javax.swing.JFileChooser jFileChooser1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItem2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JToggleButton lapiz;
    private javax.swing.JToggleButton linea;
    private javax.swing.JToggleButton serendipia;
    private javax.swing.JToggleButton triangulo;
    // End of variables declaration//GEN-END:variables
}
