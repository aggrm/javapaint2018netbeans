/*
 * esta clase dibuja Pentágonos (o eso creo)

 */
package codigo;

import java.awt.Color;

/**
 *
 * @author Alberto Goujon
 */
public class Decagono extends Forma {
    
    public Decagono(int _posX, int _posY, Color _color, boolean _relleno) {
        //Inicializa el constructor del decagono correctamente para que guarde 10 lados
        super(_posX, _posY, 10, _color, _relleno);
    }
}
